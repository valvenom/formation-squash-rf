*** Settings ***
Library    SeleniumLibrary
Resource	squash_resources.resource

*** Variables ***
${url}     https://katalon-demo-cura.herokuapp.com
${browser}    firefox
${login}    John Doe
${password}    ThisIsNotAPassword

*** Keywords ***
Test Setup
    Open Browser        ${url}    ${browser}
    Maximize Browser Window

Test Teardown
	Close Browser

*** Test Cases ***
Test 3
	[Setup]	Test Setup

	Given L'utilisateur est sur la page d'accueil
	When L'utilisateur souhaite prendre un rendez-vous
	Then La page de connexion s'affiche
	When L'utilisateur se connecte    ${login}    ${password}
	Then L'utilisateur est connecté sur la page de rendez-vous

	[Teardown]	Test Teardown